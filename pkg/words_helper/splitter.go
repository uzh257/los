package words_helper

import (
	"regexp"
	"strings"
)

type WordElement struct {
	Word      string
	Delimiter string
}

type Splitter struct {
}

func (s Splitter) SplitWords(message string) []WordElement {
	first := true
	result := make([]WordElement, 0)
	for _, word := range s.draftSplit(message) {
		if first {
			result = append(result, s.splitDelimiter(word, true))
			first = false

			continue
		}

		result = append(result, s.splitDelimiter(word, false))
	}

	return result
}

func (s Splitter) draftSplit(text string) []string {
	a := regexp.MustCompile(`(.?\s*[a-zA-Z\dа-яА-Я]+)`)
	return a.FindAllString(text, -1)
}

func (Splitter) splitDelimiter(wordWithDelimiter string, isFirst bool) WordElement {
	asRunes := []rune(wordWithDelimiter)

	totalLen := len(asRunes)
	if totalLen <= 1 {
		return WordElement{Word: wordWithDelimiter}
	}

	if isFirst {
		return WordElement{Word: wordWithDelimiter}
	}

	elements := strings.Fields(wordWithDelimiter)
	word := elements[len(elements)-1]
	wordAsRune := []rune(word)
	wordLen := len(wordAsRune)
	noWordLen := totalLen - wordLen

	return WordElement{Word: word, Delimiter: string(asRunes[:noWordLen])}
}
