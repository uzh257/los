package words_helper

import (
	"reflect"
	"testing"
)

type Splitter_SplitWords_case struct {
	name     string
	text     string
	expected []WordElement
}

func TestSplitter_SplitWords(t *testing.T) {
	var actual []WordElement
	target := Splitter{}
	for _, currentCase := range DataProviderFor_Splitter_SplitWords() {
		actual = target.SplitWords(currentCase.text)
		if !reflect.DeepEqual(currentCase.expected, actual) {
			t.Errorf(
				"Failed test \"%s\". case \"%s\"\n.Expected - %v\nActual   - %v",
				"TestSplitter_SplitWords",
				currentCase.name,
				currentCase.expected,
				actual,
			)
		}
	}
}

func DataProviderFor_Splitter_SplitWords() [2]Splitter_SplitWords_case {
	var cases [2]Splitter_SplitWords_case

	oneLineMessage := make([]WordElement, 0)
	cases[0] = Splitter_SplitWords_case{
		name: "one line message",
		text: "Доброе утро. С сегодняшнего дня отпуска проводит робот. если у кого стоят некорректные даты, просьба сегодня откорректировать .",
		expected: append(
			oneLineMessage,
			WordElement{"Доброе", ""},
			WordElement{"утро", " "},
			WordElement{"С", ". "},
			WordElement{"сегодняшнего", " "},
			WordElement{"дня", " "},
			WordElement{"отпуска", " "},
			WordElement{"проводит", " "},
			WordElement{"робот", " "},
			WordElement{"если", ". "},
			WordElement{"у", " "},
			WordElement{"кого", " "},
			WordElement{"стоят", " "},
			WordElement{"некорректные", " "},
			WordElement{"даты", " "},
			WordElement{"просьба", ", "},
			WordElement{"сегодня", " "},
			WordElement{"откорректировать", " "},
		),
	}

	multipleLineMessage := make([]WordElement, 0)
	cases[1] = Splitter_SplitWords_case{
		name: "multiple line message",
		text: "СмеркЛОСЬ думаЛОСЬ  мечтаЛОСЬ\n\tСниЛОСЬ мниЛОСЬ и икаЛОСЬ\n\tОтчего-то мне проснуЛОСЬ\n\tПо кукурузе побежаЛОСЬ",
		expected: append(
			multipleLineMessage,
			WordElement{"СмеркЛОСЬ", ""},
			WordElement{"думаЛОСЬ", " "},
			WordElement{"мечтаЛОСЬ", "  "},
			WordElement{"СниЛОСЬ", "\n\t"},
			WordElement{"мниЛОСЬ", " "},
			WordElement{"и", " "},
			WordElement{"икаЛОСЬ", " "},
			WordElement{"Отчего", "\n\t"},
			WordElement{"-то", ""},
			WordElement{"мне", " "},
			WordElement{"проснуЛОСЬ", " "},
			WordElement{"По", "\n\t"},
			WordElement{"кукурузе", " "},
			WordElement{"побежаЛОСЬ", " "},
		),
	}

	return cases
}
