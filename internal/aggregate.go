package internal

import (
	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"

	"code/internal/fixer_handler"
)

type Handler interface {
	IsSuitable(update tg.Update) bool
	Handle(update tg.Update, bot *tg.BotAPI) tg.Chattable
}

type LosBot struct {
}

func (los LosBot) Handle(update tg.Update, bot *tg.BotAPI) tg.Chattable {
	losHandler := fixer_handler.Los_Handler{Target: "ЛОСЬ", NewMessage: ""}
	currentHandler := los.chooseHandler(update, &losHandler)
	if currentHandler == nil {
		return nil
	}

	h := *currentHandler

	return h.Handle(update, bot)
}

func (LosBot) chooseHandler(update tg.Update, handlers ...Handler) *Handler {
	for _, handler := range handlers {
		if !handler.IsSuitable(update) {
			continue
		}

		return &handler
	}

	return nil
}
