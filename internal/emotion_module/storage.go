package emotion_module

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
)

type FileStorage struct {
	Path string
}

func (s FileStorage) Get(chatId int64) (int, time.Time) {
	err, fileContent := s.getFileContent(s.getFileName(chatId))
	if err != nil {
		return 0, time.Time{}
	}

	timeString := fileContent[0]
	timeInt, err := strconv.ParseInt(timeString, 10, 64)
	if err != nil {
		return 0, time.Time{}
	}

	valueString := fileContent[1]
	value, err := strconv.Atoi(valueString)
	if err != nil {
		return 0, time.Time{}
	}

	return value, time.Unix(timeInt, 0)
}

func (s FileStorage) Set(chatId int64, value int, time time.Time) {
	key := s.getFileName(chatId)
	data := make([]string, 0)
	data = append(data, fmt.Sprintf("%v", time.Unix()))
	data = append(data, fmt.Sprintf("%v", value))

	if _, err := os.Stat(key); err == nil {
		_ = os.Remove(key)
	}

	file, err := os.Create(key)
	if err != nil {
		return
	}
	defer file.Close()

	dataToSave := strings.Join(data, "\n")
	dataRaw := []byte(dataToSave)
	_, err = file.Write(dataRaw)
}

func (s FileStorage) getFileName(chatId int64) string {
	return s.Path + fmt.Sprintf("%v", chatId) + ".txt"
}

func (s FileStorage) getFileContent(key string) (error, []string) {
	file, err := os.Open(key)
	if err != nil {
		return err, nil
	}
	defer file.Close()

	fContent, err := ioutil.ReadFile(key)
	if err != nil {
		return err, nil
	}

	source := string(fContent)
	separated := strings.Split(source, "\n")

	return nil, separated
}
