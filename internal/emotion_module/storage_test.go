package emotion_module

import (
	"os"
	"testing"
	"time"
)

type FileStorage_case struct {
	name       string
	chatId     int64
	value      int
	storedTime time.Time
}

func TestFileStorage(t *testing.T) {
	var actualValue int
	var actualTime time.Time
	target := FileStorage{
		Path: "testStorage",
	}

	for _, currentCase := range DataProviderFor_FileStorage() {
		target.Set(currentCase.chatId, currentCase.value, currentCase.storedTime)
		actualValue, actualTime = target.Get(currentCase.chatId)
		if actualValue != currentCase.value {
			t.Errorf(
				"Failed test \"%s\". case \"%s\". Wrong value: expected \"%v\", got \"%v\"",
				"TestFileStorage",
				currentCase.name,
				currentCase.value,
				actualValue,
			)
		}

		if actualTime.Unix() != currentCase.storedTime.Unix() {
			t.Errorf(
				"Failed test \"%s\". case \"%s\". Wrong time: expected \"%v\", got \"%v\"",
				"TestFileStorage",
				currentCase.name,
				currentCase.storedTime.Unix(),
				actualTime.Unix(),
			)
		}
	}

	os.Remove("testStorage1.txt")
	os.Remove("testStorage2.txt")
	os.Remove("testStorage3.txt")
}

func DataProviderFor_FileStorage() [5]FileStorage_case {
	var cases [5]FileStorage_case

	testingTime := time.Now()
	cases[0] = FileStorage_case{
		name:       "usual",
		chatId:     1,
		value:      0,
		storedTime: testingTime,
	}
	cases[1] = FileStorage_case{
		name:       "positive value",
		chatId:     1,
		value:      10,
		storedTime: testingTime,
	}
	cases[2] = FileStorage_case{
		name:       "negative value",
		chatId:     1,
		value:      -1,
		storedTime: testingTime,
	}
	cases[3] = FileStorage_case{
		name:       "another chat",
		chatId:     2,
		value:      -2,
		storedTime: testingTime,
	}
	testingTime2 := testingTime.Add(10)
	cases[4] = FileStorage_case{
		name:       "another time",
		chatId:     3,
		value:      -4,
		storedTime: testingTime2,
	}

	return cases
}
