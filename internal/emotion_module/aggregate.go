package emotion_module

import (
	"log"
	"os"

	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type Emotions struct {
	Bot     *tg.BotAPI
	Update  tg.Update
	Storage StorageInterface
	Fader   FadeCalculatorInterface
}

func (e Emotions) Handle(fixes int, correct int) {
	emotionCalculator := EmotionCalculator{
		Chat:    e.Update.Message.Chat.ID,
		Storage: FileStorage{Path: os.Getenv("STORAGE_DIRECTORY")},
		Fader:   Linear{Ratio: 0.5, Unit: 600},
	}
	ok, emotionValue := emotionCalculator.GetEmotion(fixes, correct)
	if !ok {
		return
	}

	picChooser := PicChooser{}
	isPicOk, pic := picChooser.GetMessageWithPic(emotionValue)
	if !isPicOk {
		return
	}

	pic.ReplyToMessageID = e.Update.Message.MessageID
	pic.ChatID = e.Update.Message.Chat.ID

	_, mesErr := e.Bot.Send(pic)
	if mesErr != nil {
		log.Println(mesErr)
	}
}
