package emotion_module

import (
	"time"
)

type StorageInterface interface {
	Get(chatId int64) (value int, time time.Time)
	Set(chatId int64, value int, time time.Time)
}

type FadeCalculatorInterface interface {
	CalculateFading(value int, timeDelta int64) int
}

type EmotionCalculator struct {
	Chat    int64
	Storage StorageInterface
	Fader   FadeCalculatorInterface
}

func (e EmotionCalculator) GetEmotion(fixes int, correct int) (ok bool, emotion int) {
	currentTime := time.Now()

	lastEmotions, lastTime := e.Storage.Get(e.Chat)
	timeDelta := currentTime.Unix() - lastTime.Unix()
	if timeDelta < 0 {
		return false, 0
	}

	currentEmotion := e.Fader.CalculateFading(lastEmotions, timeDelta)
	newEmotion := e.calculateNewEmotion(currentEmotion, fixes, correct)

	e.Storage.Set(e.Chat, newEmotion, currentTime)

	return true, newEmotion
}

func (EmotionCalculator) calculateNewEmotion(currentEmotion int, fixes int, correct int) int {
	delta := correct - fixes
	newEmotion := currentEmotion + delta
	if newEmotion > 10 {
		return 10
	}

	if newEmotion < -10 {
		return -10
	}

	return newEmotion
}
