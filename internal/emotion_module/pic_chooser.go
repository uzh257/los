package emotion_module

import (
	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type PicChooser struct {
}

func (c PicChooser) GetMessageWithPic(em int) (found bool, message tg.PhotoConfig) {
	messageMap := getMap()
	message, found = messageMap[em]

	return
}

func getMap() map[int]tg.PhotoConfig {
	m := make(map[int]tg.PhotoConfig)
	m[-10] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE5GLqAAH4xvTW_vn7GNu-7QjSvdLSpwACzK0xG76PVVO0KBKZeBw0AgEAAwIAA20AAykE"))
	m[-9] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE_GLqBB6Bd5HEVPF2XVOXfv3Y-hZFAAJwrjEbwYRVUxiT_ziC4PYVAQADAgADbQADKQQ"))
	m[-8] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE5WLqAAH5lr7aaTWlF1qlEqY6wStbZgACdRU6G3UbZAfzfyDZu3ErvAEAAwIAA20AAykE"))
	m[-7] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE5mLqAAH5AjXTVcuBomEUPWiSeei60wACm64xGzDFVVODRoTSyoigfgEAAwIAA20AAykE"))
	m[-6] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE52LqAAH563KGsQT7tM67hEu7CI2dCAAC1q0xG9pWVFONhVgTXYWiCQEAAwIAA20AAykE"))
	m[-5] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE6GLqAAH5zTmfeFiM3YodjpJRv7YBBQACxq4xG1zXVVOacPm7LyeCngEAAwIAA20AAykE"))
	m[-4] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE6WLqAAH67MpqrbmiuQ02YqfHp6bdHgACHa4xG-VgVVMeClEUYXAecQEAAwIAA20AAykE"))
	m[-3] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE6mLqAAH-kaxdQSzqI6sbM0kov6pM8wACGK4xGxkHVFOxSgAB1V-exRQBAAMCAANtAAMpBA"))
	m[-2] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE62LqAAH-Bz5zw8aaEqNeYmcgw5i_mAACua4xG3wxVVNuDKUXUkwpbgEAAwIAA20AAykE"))
	m[-1] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE7GLqAAH-ESIP0Hd5O2bfBi5U7AZ9DwACv64xG01VVFMs6uUv4n27kwEAAwIAA20AAykE"))
	m[0] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE7WLqAAH-BFm72Br_YEyXBl0ihNNf7AAC160xG2yMVVMRlxVv19IfSgEAAwIAA20AAykE"))
	m[1] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE7mLqAAH_xgxROEZUuo1wbosIOgab6wACyq0xG1UlVFN8PuBT8i6BkgEAAwIAA20AAykE"))
	m[2] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE72LqAQZxxZXQYT1jvk8NpfghIZKGAAIFrjEbFxJVUyKvwtyuqMVkAQADAgADbQADKQQ"))
	m[3] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE8GLqAQYcmWkAAW1epj9Regs9n5JIuwACo64xGyGWVVO8RjsFQW1rbgEAAwIAA20AAykE"))
	m[4] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE8WLqAQbICnBdFQgmpRyZ2k14H8qFAALcrTEbM7xUU7gULtsrcxBcAQADAgADbQADKQQ"))
	m[5] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE8mLqAQd6Zeowy2uPwWzyJRIIAgSLAAL1qjEb9-e9UEiatI-KfraPAQADAgADbQADKQQ"))
	m[6] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE82LqAQePF4vvKv9i-EheyRZ-0LumAAJUrjEbDo1UU7AjK05PfDKhAQADAgADbQADKQQ"))
	m[7] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE9GLqAQvq6wrvLm3vEPbn8dHq8R-GAAIMrjEb41NVU-f64DHwBsIhAQADAgADbQADKQQ"))
	m[8] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE9WLqAQswpXwfFyHTFb0TK1MsJAK5AAInrjEbw71UU0MpY044eZ9iAQADAgADbQADKQQ"))
	m[9] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE9mLqAQvYbpJqqpPZ4MdgFXIPtGOnAAK1rjEb-dBVU1XDWAX6IHvhAQADAgADbQADKQQ"))
	m[10] = tg.NewPhoto(0, tg.FileID("AgACAgQAAxkDAAIE92LqAQsKDD9UyXvHuaUm9zJYXOW0AALnrTEbr-ZUUwHIuddASI_yAQADAgADbQADKQQ"))

	return m
}
