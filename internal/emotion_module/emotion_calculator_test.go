package emotion_module

import (
	"testing"
)

type EmotionCalculator_CalculateNewEmotion_case struct {
	Name            string
	CurrentEmotions int
	Fixes           int
	Corrects        int
	Expect          int
}

func TestEmotionCalculator_CalculateNewEmotion(t *testing.T) {
	var actual int
	target := EmotionCalculator{}
	for _, currentCase := range DataProviderFor_EmotionCalculator_CalculateNewEmotion() {
		actual = target.calculateNewEmotion(
			currentCase.CurrentEmotions,
			currentCase.Fixes,
			currentCase.Corrects,
		)

		if actual == currentCase.Expect {
			continue
		}

		t.Errorf(
			"Failed test \"%s\". case \"%s\". Expected \"%v\", got \"%v\"",
			"TestEmotionCalculator_CalculateNewEmotion",
			currentCase.Name,
			currentCase.Expect,
			actual,
		)
	}
}

func DataProviderFor_EmotionCalculator_CalculateNewEmotion() [12]EmotionCalculator_CalculateNewEmotion_case {
	var cases [12]EmotionCalculator_CalculateNewEmotion_case

	cases[0] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "zero changes",
		CurrentEmotions: 1,
		Fixes:           1,
		Corrects:        1,
		Expect:          1,
	}
	cases[1] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "zero changes from positive",
		CurrentEmotions: 10,
		Fixes:           1,
		Corrects:        1,
		Expect:          10,
	}
	cases[2] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "zero changes from negative",
		CurrentEmotions: -8,
		Fixes:           1,
		Corrects:        1,
		Expect:          -8,
	}
	cases[3] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "zero summ",
		CurrentEmotions: 1,
		Fixes:           6,
		Corrects:        6,
		Expect:          1,
	}
	cases[4] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "zero summ from positive",
		CurrentEmotions: 10,
		Fixes:           6,
		Corrects:        6,
		Expect:          10,
	}
	cases[5] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "zero summ from negative",
		CurrentEmotions: -8,
		Fixes:           6,
		Corrects:        6,
		Expect:          -8,
	}
	cases[6] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "more fixes",
		CurrentEmotions: 1,
		Fixes:           8,
		Corrects:        4,
		Expect:          -3,
	}
	cases[7] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "more fixes from positive",
		CurrentEmotions: 10,
		Fixes:           8,
		Corrects:        4,
		Expect:          6,
	}
	cases[8] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "more fixes from negative",
		CurrentEmotions: -5,
		Fixes:           8,
		Corrects:        4,
		Expect:          -9,
	}
	cases[9] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "more corrects",
		CurrentEmotions: 1,
		Fixes:           6,
		Corrects:        9,
		Expect:          4,
	}
	cases[10] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "more corrects from positive",
		CurrentEmotions: 8,
		Fixes:           6,
		Corrects:        9,
		Expect:          10,
	}
	cases[11] = EmotionCalculator_CalculateNewEmotion_case{
		Name:            "more corrects from negative",
		CurrentEmotions: -8,
		Fixes:           6,
		Corrects:        9,
		Expect:          -5,
	}

	return cases
}
