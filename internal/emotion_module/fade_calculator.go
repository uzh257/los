package emotion_module

import (
	"math"
)

type Linear struct {
	Ratio float64
	Unit  int
}

func (f Linear) CalculateFading(value int, timeDelta int64) int {
	units := float64(timeDelta) / float64(f.Unit)
	delta := units * f.Ratio

	if value > 0 {
		result := float64(value) - delta
		if result < 0 {
			return 0
		}

		return int(math.Round(result))
	}

	if value < 0 {
		result := float64(value) + delta
		if result > 0 {
			return 0
		}

		return int(math.Round(result))
	}

	return 0
}
