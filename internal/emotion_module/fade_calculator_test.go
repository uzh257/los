package emotion_module

import (
	"testing"
)

type Linear_CalculateFading_case struct {
	Name      string
	value     int
	timeDelta int64
	expected  int
}

func TestLinear_CalculateFading(t *testing.T) {
	target := Linear{Ratio: 0.5, Unit: 600}
	for _, currentCase := range DataProviderFor_Linear_CalculateFading() {
		actual := target.CalculateFading(currentCase.value, currentCase.timeDelta)
		if actual == currentCase.expected {
			continue
		}

		t.Errorf(
			"Failed test \"%s\". case \"%s\". Expected \"%v\", got \"%v\"",
			"TestLinear_CalculateFading",
			currentCase.Name,
			currentCase.expected,
			actual,
		)
	}

}

func DataProviderFor_Linear_CalculateFading() [5]Linear_CalculateFading_case {
	var cases [5]Linear_CalculateFading_case

	cases[0] = Linear_CalculateFading_case{
		Name:      "empty",
		value:     0,
		timeDelta: 0,
		expected:  0,
	}

	cases[1] = Linear_CalculateFading_case{
		Name:      "from max",
		value:     10,
		timeDelta: 1900,
		expected:  8,
	}

	cases[2] = Linear_CalculateFading_case{
		Name:      "from max sub zero",
		value:     10,
		timeDelta: 12900,
		expected:  0,
	}

	cases[3] = Linear_CalculateFading_case{
		Name:      "from min",
		value:     -10,
		timeDelta: 1900,
		expected:  -8,
	}

	cases[4] = Linear_CalculateFading_case{
		Name:      "from min above zero",
		value:     -10,
		timeDelta: 12900,
		expected:  0,
	}

	return cases
}
