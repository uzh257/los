package fixer_handler

import (
	"testing"
)

type Los_Handler_FixWord_case struct {
	word              string
	expectedIsLos     bool
	expectedIsCorrect bool
	expectedFixed     string
}

func TestLos_Handler_FixWord(t *testing.T) {
	var actualIsLos bool
	var actualIsCorrect bool
	var actualFixed string

	target := Los_Handler{Target: "ЛОСЬ"}
	for _, currentCase := range DataProviderFor_Los_Handler_FixWord() {
		actualIsLos, actualIsCorrect, actualFixed = target.FixWord(currentCase.word)
		if actualIsLos != currentCase.expectedIsLos {
			t.Errorf(
				"Failed test \"%s\". case \"%s\". Wrong isLos",
				"TestLos_Handler_FixWord",
				currentCase.word,
			)
		}

		if actualIsCorrect != currentCase.expectedIsCorrect {
			t.Errorf(
				"Failed test \"%s\". case \"%s\". Wrong isCorrect",
				"TestLos_Handler_FixWord",
				currentCase.word,
			)
		}

		if actualFixed != currentCase.expectedFixed {
			t.Errorf(
				"Failed test \"%s\". case \"%s\". Wrong fixed",
				"TestLos_Handler_FixWord",
				currentCase.word,
			)
		}
	}
}

func DataProviderFor_Los_Handler_FixWord() [7]Los_Handler_FixWord_case {
	var cases [7]Los_Handler_FixWord_case

	cases[0] = Los_Handler_FixWord_case{"отпуска", false, false, "отпуска"}
	cases[1] = Los_Handler_FixWord_case{"отпускалось", true, false, "отпускаЛОСЬ"}
	cases[2] = Los_Handler_FixWord_case{"отпускаЛОСЬ", true, true, "отпускаЛОСЬ"}
	cases[3] = Los_Handler_FixWord_case{"лось", true, false, "ЛОСЬ"}
	cases[4] = Los_Handler_FixWord_case{"ЛОСЬ", true, true, "ЛОСЬ"}
	cases[5] = Los_Handler_FixWord_case{"", false, false, ""}
	cases[6] = Los_Handler_FixWord_case{"1", false, false, "1"}

	return cases
}
