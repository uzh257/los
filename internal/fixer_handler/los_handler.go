package fixer_handler

import (
	"os"
	"strings"

	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"

	em "code/internal/emotion_module"
	"code/pkg/words_helper"
)

type Handler interface {
	IsSuitable(update tg.Update) bool
	Handle(update tg.Update, bot *tg.BotAPI) tg.Chattable
}

type Los_Handler struct {
	Target     string
	NewMessage string
	fixes      int
	corrects   int
}

func (l *Los_Handler) IsSuitable(update tg.Update) bool {
	var isSuitable bool
	wordHelper := words_helper.Splitter{}
	for _, word := range wordHelper.SplitWords(update.Message.Text) {
		isLos, isCorrect, fixedWord := l.FixWord(word.Word)

		l.NewMessage += word.Delimiter
		l.NewMessage += fixedWord

		if !isLos {
			continue
		}

		isSuitable = true

		if isCorrect {
			l.corrects++
		} else {
			l.fixes++
		}
	}

	return isSuitable
}

func (l *Los_Handler) Handle(update tg.Update, bot *tg.BotAPI) tg.Chattable {
	var message tg.MessageConfig
	if l.fixes > 0 {
		message = tg.NewMessage(
			update.Message.Chat.ID,
			l.NewMessage,
		)
		message.ReplyToMessageID = update.Message.MessageID
	}

	e := em.Emotions{
		Bot:    bot,
		Update: update,
		Storage: em.FileStorage{
			Path: os.Getenv("STORAGE_DIRECTORY"),
		},
		Fader: em.Linear{
			Ratio: 1 / 2,
			Unit:  60 * 10,
		},
	}
	go e.Handle(l.fixes, l.corrects)

	return message
}

func (l *Los_Handler) FixWord(word string) (isLos bool, isCorrect bool, fixed string) {
	isLos = false
	isCorrect = false
	fixed = word

	wordAsRune := []rune(word)
	wordLen := len(wordAsRune)

	targetAsRune := []rune(l.Target)
	targetLen := len(targetAsRune)

	wordWithoutSuffixLen := wordLen - targetLen
	if wordWithoutSuffixLen < 0 {
		return
	}

	suffixAsRune := wordAsRune[wordWithoutSuffixLen:]
	suffix := string(suffixAsRune)

	isLos = (strings.ToLower(suffix) == strings.ToLower(l.Target))
	if !isLos {
		return
	}

	isCorrect = (suffix == l.Target)
	if isCorrect {
		return
	}

	wordWithoutSuffixAsRune := wordAsRune[:wordWithoutSuffixLen]
	wordWithoutSuffix := string(wordWithoutSuffixAsRune)
	fixed = wordWithoutSuffix + l.Target

	return
}
