package main

import (
	"log"
	"os"

	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/joho/godotenv"

	"code/internal"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	bot, err := tg.NewBotAPI(os.Getenv("TELEGRAM_BOT_TOKEN"))
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = false

	bot.DeleteWebhook()

	u := tg.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)
	for update := range updates {
		if update.Message == nil {
			continue
		}

		go handle(update, bot)
	}
}

func handle(update tg.Update, bot *tg.BotAPI) {
	botLogic := internal.LosBot{}
	newMessage := botLogic.Handle(update, bot)
	if newMessage == nil {
		return
	}

	_, mesErr := bot.Send(newMessage)
	if mesErr != nil {
		log.Println(mesErr)
	}
}
